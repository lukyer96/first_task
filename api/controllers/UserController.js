/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `UserController.GetAllContacts()`
   */
  GetAllContacts: function (req,res) {
    
    UserService.GetAllContacts(function(err, records) {
    if (!err) {
      return res.json(records);
    }else{
      return res.notFound();
    }
    });
    
  },


  /**
   * `UserController.AddContact()`
   */
  AddContact: function (req, res) {


    UserService.AddContact({name: req.param('name'),phone_number:req.param('phone_number')},function(err,message) {
    if (!err) {
      return res.send(message);
    }else{
      return res.notFound();
    }
   });    


   
 }
};

